package com.example.appnavigationdrawer

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.appnavigationdrawer.adapters.MenuAdapter
import com.example.appnavigationdrawer.databinding.ActivityMainBinding
import com.example.appnavigationdrawer.models.MenuItems
import kotlinx.coroutines.InternalCoroutinesApi

class MainActivity : AppCompatActivity() {
    private val viewModel: CoroutineViewmodel by viewModels()

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivityMainBinding
    private lateinit var myAdapter: MenuAdapter
    private var itemsList = mutableListOf<MenuItems>()

    @InternalCoroutinesApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        viewModel.startRepeatingJob(5000L)
        addMenu()
        setSupportActionBar(binding.appBarMain.toolbar)

        val navController = findNavController(R.id.nav_host_fragment_content_main)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_gallery
            ), binding.drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        binding.navView.setupWithNavController(navController)
    }


    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment_content_main)
        myAdapter.fixBack()
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    private fun addMenu() {

        val navController = findNavController(R.id.nav_host_fragment_content_main)

        itemsList.add(MenuItems(R.mipmap.ic_launcher_round, "Gallery"))
        itemsList.add(MenuItems(R.mipmap.ic_launcher_round, "Home"))
        itemsList.add(MenuItems(R.mipmap.ic_launcher_round, "SlideShow"))

        myAdapter = MenuAdapter(itemsList)
        binding.menuRecycler.layoutManager = LinearLayoutManager(this)
        myAdapter.menuClick = {
            binding.drawerLayout.closeDrawer(GravityCompat.START)
            when (it) {
                0 -> navController.navigate(R.id.nav_gallery)
                1 -> navController.navigate(R.id.nav_home)
                2 -> navController.navigate(R.id.nav_slideshow)
            }
        }
        binding.menuRecycler.adapter = myAdapter
    }
}