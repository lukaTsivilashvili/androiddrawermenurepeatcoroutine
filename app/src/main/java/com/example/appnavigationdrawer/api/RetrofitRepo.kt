package com.example.appnavigationdrawer.api

import com.example.appnavigationdrawer.models.CountryModel
import retrofit2.Response
import retrofit2.http.GET

interface RetrofitRepo {
    @GET("rest/v2/all")
    suspend fun getCountry(): Response<List<CountryModel>>
}