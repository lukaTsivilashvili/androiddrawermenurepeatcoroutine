package com.example.appnavigationdrawer.models

data class CountryModel(
    val name: String? = null,
    val region: String,
    val nativeName:String,
    val flag:String
)
