package com.example.appnavigationdrawer.models

data class MenuItems(
    val image:Int,
    val title:String
)
