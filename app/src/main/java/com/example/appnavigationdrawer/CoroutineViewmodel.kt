package com.example.appnavigationdrawer

import android.util.Log.d
import androidx.lifecycle.ViewModel
import com.example.appnavigationdrawer.api.RetrofitService
import kotlinx.coroutines.*

class CoroutineViewmodel:ViewModel() {

    @InternalCoroutinesApi
    fun startRepeatingJob(timeInterval: Long): Job {
        return CoroutineScope(Dispatchers.Default).launch {
            while (NonCancellable.isActive) {

                val result = RetrofitService.service.getCountry()

                if (result.isSuccessful) {
                    val items = result.body()
                    d("testLog", items.toString())
                }
                delay(timeInterval)
            }
        }
    }


}