package com.example.appnavigationdrawer.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.appnavigationdrawer.R
import com.example.appnavigationdrawer.databinding.ItemMenuBinding
import com.example.appnavigationdrawer.models.MenuItems

typealias menuClicker = (position:Int) -> Unit

class MenuAdapter(private val items: MutableList<MenuItems>) :
    RecyclerView.Adapter<MenuAdapter.MenuViewHolder>() {

    lateinit var menuClick: menuClicker
    var selposition = 0

    inner class MenuViewHolder(val binding: ItemMenuBinding) :
        RecyclerView.ViewHolder(binding.root), View.OnClickListener {

        private lateinit var model: MenuItems


        fun onBind() {

            model = items[adapterPosition]

            binding.imageMenu.setImageResource(model.image)
            binding.titleTv.text = model.title
            binding.root.setOnClickListener(this)

            if (adapterPosition == selposition){
                binding.root.setBackgroundColor(ContextCompat.getColor(binding.root.context, R.color.purple_200))
                binding.selectedView.visibility = View.VISIBLE
            }else{
                binding.selectedView.visibility = View.INVISIBLE
                binding.root.setBackgroundColor(ContextCompat.getColor(binding.root.context, R.color.white))

            }

        }


        override fun onClick(v: View?) {
            selposition = adapterPosition
            menuClick.invoke(adapterPosition)
            notifyDataSetChanged()
        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = MenuViewHolder(
        ItemMenuBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    )

    override fun onBindViewHolder(holder: MenuViewHolder, position: Int) {
        holder.onBind()
    }

    override fun getItemCount() = items.size

    fun fixBack(){

        selposition = 0
        notifyDataSetChanged()

    }

}